# Release 1.0.3 - Fri Oct 19 05:58:00 EST 2016

- Fixed for blank=True field (Stanislav Davydov)

# Release 1.0.2 - Fri May  9 12:11:15 EST 2014

- Modifications to README and setup

# Release 1.0.1 - Fri May  9 11:25:43 EST 2014

- Modifications to README

# Release 1.0.0 - Fri May  9 11:14:55 EST 2014

- Initial release

