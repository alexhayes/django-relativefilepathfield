# Django RelativeFilePathField

As the name suggests, RelativeFilePathField is a [FilePathField](https://docs.djangoproject.com/en/dev/ref/models/fields/#filepathfield)
which stores a relative value to the file.

[For reasons](https://code.djangoproject.com/ticket/6896#comment:11), which I do 
not agree with, the developers of Django will not add an option to FilePathField to 
support relative paths.

The code in this project originated from [this patch](https://code.djangoproject.com/ticket/6896#comment:4)
created by seniorquico.

# Installation

```bash
pip install django-relativefilepathfield
```

or, from source;

```bash
pip install git+https://bitbucket.org/alexhayes/django-relativefilepathfield.git
```

# Usage

RelativeFilePathField accepts the same options as [FilePathField](https://docs.djangoproject.com/en/dev/ref/models/fields/#filepathfield)
and is essentially a drop in replacement, thus;

```python
class Template(models.Model):
	file = models.FilePathField(path='/path/to/templates')
```

becomes;

```python
from relativefilepathfield.fields import RelativeFilePathField

class MyModel(models.Model):
	file = RelativeFilePathField(path='/path/to/templates')
```

# API

## get_FOO_abspath()

For every `RelativeFilePathField`, a model instance will have a `get_FOO_abspath()` method,
where `FOO` is the name of the field. This method returns the absolute path of the
field.

For example:

```python
class Template(models.Model):
	file = RelativeFilePathField(path='/path/to/templates')
```

```python
>>> t = Template(file='example.html')
>>> t.file
'example.html'
>>> t.get_file_abspath()
'/path/to/templates/example.html'
```

# Tests

If anyone would care to fork and create some tests that would be very much 
appreciated.

There are tests within the [submitted patch](https://code.djangoproject.com/attachment/ticket/6896/6896.diff)
referred to above however I have not yet had time to integrate them into the project.

If you are interested in creating some tests to be contained within the app I do 
have a few tests within the project I needed to use this, so get in contact and I
can send you these.

# Author

- Alex Hayes <alex@alution.com> (zanuxzan)
- seniorquico (on https://code.djangoproject.com)
